/**
 * Created by abner on 13/07/15.
 */
/// <reference path='../_MyApp.ts' />

module MyApp {
    'use strict';

    export class TodoItem {
        constructor(
            public title: string,
            public completed: boolean
        ) { }
    }
}
