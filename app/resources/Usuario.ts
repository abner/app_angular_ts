/// <reference path="../_MyApp.ts" />
module MyApp.Resources
{
    'use strict';

    export interface IUsuario {
        id: number;
        nome: String;
        active: boolean;
    }

    export class Usuario implements IUsuario {

        id: number;
        nome: String;
        active: boolean;
        constructor(id: number, nome: String, active: boolean )
        {
            this.id = id;
            this.nome = nome;
            this.active = active;
        }



    }


}
