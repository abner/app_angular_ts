/// <reference path="../_MyApp.ts" />
/// <reference path="../MyApp.module.ts" />
module controllers
{
	export interface IFormularioController
	{
		nome: String;
    	idade: Number;
		email: String;
		toString()
	}

	export class FormularioController implements IFormularioController {
		//region implementation of IFormularioController
		nome: String;
		idade: Number;
    email: String;
		toString()
		{
			var nome = this.nome;
      //Interpolação de String em TypeScript
      return  "Nome: " + this.nome + "\
			  \nEmail: " + this.email	  + "\
			  \nIdade: " + this.idade;
		}


		//endregion

		public static $inject = ['logger'];
		constructor(logger: ng.ILogService)
		{
			var vm = <IFormularioController>this;
			vm.nome = "Ábner Oliveira";//
			vm.idade = 33;
      		vm.email = "";
			logger.info('FormularioController criado!');
		}


	}

	//this call has to be at the bottom
	//angular.module('app').controller('shell', Shell);
	MyApp.module.register.controller('formularioController', FormularioController);

}
