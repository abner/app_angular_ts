/// <reference path='../_MyApp.ts' />
/// <reference path="../MyApp.module.ts" />
module MyApp {
    'use strict';

    /**
     * Services that persists and retrieves TODOs from localStorage.
     */
    export class TodoStorage implements ITodoStorage {

        STORAGE_ID = 'app_angular_ts';

        get (): TodoItem[] {
            return JSON.parse(localStorage.getItem(this.STORAGE_ID) || '[]');
        }

        put(todos: TodoItem[]) {
            localStorage.setItem(this.STORAGE_ID, JSON.stringify(todos));
        }
    }

    MyApp.module.register.service('todoStorage', TodoStorage);
}