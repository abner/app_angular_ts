/// <reference path='../_MyApp.ts' />
/// <reference path="../MyApp.module.ts" />
module MyApp {
    'use strict';

    /**
     * Directive that executes an expression when the element it is applied to loses focus.
     */
    export function todoBlur(): ng.IDirective {
        return {
            link: ($scope: ng.IScope, element: JQuery, attributes: any) => {
                element.bind('blur', () => { $scope.$apply(attributes.todoBlur); });
                $scope.$on('$destroy', () => { element.unbind('blur'); });
            }
        };
    }


    MyApp.module.register.directive('todoBlur', todoBlur);

}
