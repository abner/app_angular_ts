/**
 * Created by abner on 12/07/15.
 */
/// <reference path='../_MyApp.ts' />

module MyApp {
    export interface ITodoStorage {
        get (): TodoItem[];
        put(todos: TodoItem[]);
    }
}
