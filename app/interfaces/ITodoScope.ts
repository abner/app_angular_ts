/// <reference path="../_MyApp.ts" />
/**
 * Created by abner on 13/07/15.
 */
module MyApp {

    export interface ITodoScope extends ng.IScope {
        todos: TodoItem[];
        newTodo: string;
        editedTodo: TodoItem;
        remainingCount: number;
        doneCount: number;
        allChecked: boolean;
        statusFilter: { completed: boolean; };
        location: ng.ILocationService;
        vm: TodoCtrl;
    }

}